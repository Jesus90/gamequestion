import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ViewpreuntasComponent } from './main/viewpreuntas/viewpreuntas.component';

const routes: Routes = [
{
  path:'',
  component: ViewpreuntasComponent,
  pathMatch:'full'
},
{
  path:'**',
  redirectTo:'',
  pathMatch:'full'
}


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
