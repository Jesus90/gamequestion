export interface Irespuestas {

    idpregunta:number,
    pregunta:string,
    respuesta:boolean,
    seleccion:string

}
