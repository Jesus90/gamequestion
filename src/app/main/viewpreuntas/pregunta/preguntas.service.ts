import { Injectable } from '@angular/core';


import { BehaviorSubject, Observable, observable } from 'rxjs';
import preguntasjson  from './preguntas.json';
import { Ipeguntas } from './ipeguntas';
import { Irespuestas } from '../respuestas/irespuestas';

@Injectable({
  providedIn: 'root'
})
export class PreguntasService {

  listarpreguntas: any[]  = [...preguntasjson]; 
  listarpreguntas$ = new BehaviorSubject<Ipeguntas[]>(this.listarpreguntas);


  respuestas:Irespuestas[]= [];



  constructor() { }


  ObtenerListarPreguntas():Observable<Ipeguntas[]> {
     return this.listarpreguntas$.asObservable();

  }



  SetRespuesta(idpregunta : number , pregunta:string  , respuesta: boolean, seleccion: string ){

    if(this.respuestas.length == 0 ) {
      this.respuestas.push({
        
        "idpregunta":idpregunta,
        "pregunta":pregunta,
        "respuesta":respuesta,
        "seleccion":seleccion
      });
    }else {
   
   
       if(!this.respuestas.find((s => s.idpregunta  === idpregunta ))){
        this.respuestas.push({
          "pregunta":pregunta,
          "idpregunta":idpregunta,
          "respuesta":respuesta,
          "seleccion":seleccion
        });

       }
        
    }


 }

getRespuesta():Irespuestas[]{
      return this.respuestas;

}


restRespustas(){

  this.respuestas = [];

}

}
